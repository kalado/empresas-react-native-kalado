![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

* Feito por: Lucas Calado Alves Pereira
* Data: 17/09/2019

### Justificativas ###

* O projeto foi feito em cima de uma base já pré-programada. Isso aconteceu pela urgência, como não pude realizar o teste utilizando os 3 dias, acabei fazendo ele em algumas horas e precisei usar as ferramentas que tinha para agilizar o processo.
* Não investi muito em interface, apesar de entender que é super importante.
* Alguns códigos foram feitos correndo, mas tentei deixar o mais arrumado possível.

### Execução do Código ###

* Execute o comando yarn
* Execute o comando yarn ios para gerar a Build para iOS
* Execute o comando yarn android para gerar a Build para Android
* No caso de testes em um iOS5s execute o comando yarn ios5s


### Agradecimentos ###

* Agradeço pela oportunidade de fazer o teste, foi bem legal a experiência. Gostaria de ter tido mais tempo  para entregar em melhor qualidade.


### Um abraço e espero que goste! :) ###

