import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator,
  NavigationRouteConfig,
  StackNavigatorConfig,
} from 'react-navigation';

import {
  WelcomeScreen, LoginScreen,
} from '~/Screens';
import { EnterpriseScreen } from '~/Screens/Enterprise';

export interface Routes {
  Welcome: NavigationRouteConfig;
  Login: NavigationRouteConfig;
  Enterprise: NavigationRouteConfig;
}

const routeConfigMap: Routes = {
  Login: {
    screen: LoginScreen,
  },
  Welcome: {
    screen: WelcomeScreen,
  },
  Enterprise: {
    screen: EnterpriseScreen,
  },
};
const routeConfig: StackNavigatorConfig = {
  headerBackTitleVisible: false,
};

const RootStackNavigator = createStackNavigator(routeConfigMap, routeConfig);

const RootNavigator = createSwitchNavigator({
  Root: RootStackNavigator,
});

const AppContainer = createAppContainer(RootNavigator);

export {
  AppContainer,
};
