import { Routes } from '.';

export type RouteNames = {
  [K in keyof Routes]: string;
};

const appRoutes: RouteNames = {
  Welcome: 'Welcome',
  Login: 'Login',
  Enterprise: 'Enterprise',
};

export {
  appRoutes,
};
