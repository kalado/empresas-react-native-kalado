import { call, put } from 'redux-saga/effects';

import { AuthApi } from '~/Services/AuthApi';
import { AuthCreators } from '~/Redux/Auth';
import { extractErrorMsgFromData } from '~/Services/Api';

export function* loginSaga(authApi: AuthApi, action: ReturnType<typeof AuthCreators.loginRequest>) {
  const credentials = action.payload;
  const { ok, data, problem, headers } = yield call(
    [authApi, authApi.login], credentials);

  if (ok && data) {
    const headerCredentials = yield extractHeaderCredentials(headers);
    yield put(AuthCreators.loginSuccess(headerCredentials, data));
  } else {
    yield put(AuthCreators.loginFailure(extractErrorMsgFromData(data, problem)));
  }
}

function* extractHeaderCredentials(headers: any) {
  return {
    client: headers['client'],
    uid: headers['uid'],
    access_token: headers['access-token'],
  };
}
