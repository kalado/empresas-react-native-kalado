import { put, call } from 'redux-saga/effects';

import {
  EnterpriseRetrieveAllActionCreators, EnterpriseRetrieveOneActionCreators,
} from '~/Redux/Enterprise/actions';
import { EnterpriseApi } from '~/Services/Api/EnterpriseApi';

export function* enterpriseRetrieveAllSaga(
  api: EnterpriseApi,
  action: ReturnType<typeof EnterpriseRetrieveAllActionCreators.enterpriseRetrieveAllRequest>,
) {
  const { payload } = action;
  const { data, ok, problem } = yield call([api, api.getEnterprises], payload);
  if (ok) {
    yield put(EnterpriseRetrieveAllActionCreators.enterpriseRetrieveAllSuccess(
      data['enterprises'],
    ));
  } else {
    const message = data && data.message ? data.message : `${problem}`;
    yield put(EnterpriseRetrieveAllActionCreators.enterpriseRetrieveAllFailure(message));
  }
}

export function* enterpriseRetrieveOneSaga(
  api: EnterpriseApi,
  action: ReturnType<typeof EnterpriseRetrieveOneActionCreators.enterpriseRetrieveOneRequest>,
) {
  const { header, enterpriseId } = action.payload;
  const { ok, data, problem } = yield call([api, api.getEnterprise], header, enterpriseId);
  if (ok) {
    yield put(EnterpriseRetrieveOneActionCreators.enterpriseRetrieveOneSuccess(data));
  } else {
    const message = data && data.message ? data.message : `${problem}`;
    yield put(EnterpriseRetrieveOneActionCreators.enterpriseRetrieveOneFailure(message));
  }
}
