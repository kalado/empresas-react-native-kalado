import { put, select } from 'redux-saga/effects';

import { AuthApi } from '~/Services/AuthApi';
import { StartupCreators } from '~/Redux/Startup';
import { AuthCreators, AuthSelectors } from '~/Redux/Auth';
import { getUserInfo } from '~/Saga/Login';

export function* startupSaga(authApi: AuthApi) {
  yield startupAuth(authApi);
  yield put(StartupCreators.startupFinish());
}

function* startupAuth(authApi: AuthApi) {
  authApi.authToken = yield select(AuthSelectors.selectAuthToken);
  const user = yield getUserInfo(authApi);
  if (user) {
    yield put(AuthCreators.loginSuccess(authApi.authToken, user));
  } else {
    yield put(AuthCreators.loginFailure(''));
  }
}
