import { all, takeLatest } from 'redux-saga/effects';

import { Apis } from '~/Services';
import { AuthTypes } from '~/Redux/Auth';
import { loginSaga } from './Login';
import { EnterpriseTypes } from '~/Redux/Enterprise/actions';
import { enterpriseRetrieveAllSaga, enterpriseRetrieveOneSaga } from './Enterprise';

export function * rootSaga(apis: Apis) {
  yield all([
    takeLatest(AuthTypes.LOGIN_REQUEST, loginSaga, apis.auth),

    takeLatest(
      EnterpriseTypes.ENTERPRISE_RETRIEVE_ALL_REQUEST,
      enterpriseRetrieveAllSaga,
      apis.enterprise,
    ),
    takeLatest(
      EnterpriseTypes.ENTERPRISE_RETRIEVE_ONE_REQUEST,
      enterpriseRetrieveOneSaga,
      apis.enterprise,
    ),
  ]);
}
