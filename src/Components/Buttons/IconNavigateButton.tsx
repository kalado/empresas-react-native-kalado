import React from 'react';
import { useNavigation } from 'react-navigation-hooks';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

interface Props {
  icon: string;
  route: string;
}

function IconNavigateButton({ icon, route }: Props) {
  const { navigate } = useNavigation();
  return (
    <Icon
      name={icon}
      color="#000"
      size={25}
      onPress={() => navigate(route)}
    />
  );
}

export {
  IconNavigateButton,
};
