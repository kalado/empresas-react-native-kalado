import React from 'react';
import { Button } from 'react-native';
import { useNavigation } from 'react-navigation-hooks';

interface Props {
  title: string;
  route: string;
}

function NavigateButton({ title, route }: Props) {
  const { navigate } = useNavigation();
  return (
    <Button title={title} onPress={() => navigate(route)} />
  );
}

export {
  NavigateButton,
};
