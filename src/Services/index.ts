import { ApiInstance, getApiInstance } from './Api';
import { getAuthApiInstance, AuthApi } from './AuthApi';
import { EnterpriseApi, getEnterpriseApiInstance } from './Api/EnterpriseApi';

export interface Apis {
  auth: AuthApi;
  enterprise: EnterpriseApi;
}

let instance: Apis;

export function getApisInstance(apiParam?: ApiInstance): Apis {
  if (!instance) {
    const api = apiParam || getApiInstance();
    instance = {
      auth: getAuthApiInstance(api),
      enterprise: getEnterpriseApiInstance(api),
    };
  }
  return instance;
}
