import apisauce, { ApisauceInstance, ApiResponse as ApisauceResponse } from 'apisauce';
import Qs from 'qs';

import { apiUrl, apiTimeoutMillis } from '~/Config';
import { AppError } from './models';

export type ApiResponse<T> = ApisauceResponse<T & Partial<AppError>>;

export type ApiInstance = ApisauceInstance;

export function createApi(baseURL: string) {
  const api = apisauce.create({
    baseURL,
    timeout: apiTimeoutMillis,
    paramsSerializer: params => (Qs.stringify(params, { arrayFormat: 'repeat' })),
  });

  api.addResponseTransform(normalizeError());

  return api;
}

let apiInstance: ApiInstance;

export function getApiInstance(baseURL: string = apiUrl) {
  if (!apiInstance) {

    if (!baseURL) {
      throw new Error('baseURL not defined');
    }
    apiInstance = createApi(baseURL);
  }
  return apiInstance;
}

function normalizeError(translateFn: (s: string) => string = s => s) {
  return (response: ApiResponse<any>) => {
    if (!response.ok) {
      if (!response.data) response.data = {};

      const data = response.data;
      if (!data.message) {
        let message: string = response.problem || 'UNKNOWN_ERROR';
        message = `error.${message}`;
        if (response.problem === 'CLIENT_ERROR') {
          switch (response.status) {
            case 400:
            case 401:
            case 403:
            case 404:
              message = `${message}_${response.status}`;
              break;
          }
        }
        try {
          message = translateFn(message);
        } catch (error) {
          console.error('Falha ao traduzir mensagem de erro da requisição');
        }
        data.message = message;
      }
    }
  };
}

export function extractErrorMsgFromData<T>(data: T | Partial<AppError>, problem: string | null) {
  let message = undefined;
  if (typeof data === 'string') {
    message = data;
  } else if (typeof data === 'object' && (data as Partial<AppError>).message) {
    message = (data as Partial<AppError>).message;
  }
  return message || problem || '';
}
