import { ApiInstance, ApiResponse, getApiInstance } from '~/Services/Api';
import { Enterprise } from '~/Redux/Enterprise/Entity';
import { HeaderCredetials } from '~/Redux/Auth';

export interface EnterpriseApi {
  getEnterprises(header: HeaderCredetials): Promise<ApiResponse<Enterprise[]>>;
  getEnterprise(header: HeaderCredetials, enterpriseId: string): Promise<ApiResponse<Enterprise>>;
}

export class EnterpriseApiImpl {
  url = 'enterprises';

  constructor(private api: ApiInstance) {}

  getEnterprises(header: HeaderCredetials): Promise<ApiResponse<Enterprise[]>> {
    this.api.setHeaders({
      ...header,
      'access-token': header.access_token,
    });
    return this.api.get(`${this.url}`);
  }

  getEnterprise(header: HeaderCredetials, enterpriseId: string): Promise<ApiResponse<Enterprise>> {
    this.api.setHeaders({
      ...header,
      'access-token': header.access_token,
    });
    return this.api.get(`${this.url}/${enterpriseId}`);
  }
}

let instance: EnterpriseApi;

export function getEnterpriseApiInstance(api: ApiInstance) {
  if (!instance) {
    const myApi = api || getApiInstance();
    instance = new EnterpriseApiImpl(myApi);
  }
  return instance;
}
