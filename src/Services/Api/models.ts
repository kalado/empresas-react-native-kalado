export interface AppError {
  message: string;
}

export interface ErrorRequestState {
  hasError: boolean;
  errorMsg: string;
}

export interface ErrorRequestStateWithData extends ErrorRequestState {
  data?: any;
}

export interface RequestState extends ErrorRequestState {
  isLoading: boolean;
  isDone?: boolean;
}

export interface Pageable {
  page: number;
  size: number;
}

export interface RequestPageableState extends RequestState {
  pageable: Pageable;
}
