import { Enterprise } from '~/Redux/Enterprise';

export interface UserInfo {
  investor: {
    id: number;
    investor_name: string;
    email: string;
    city: string;
    country: string;
    balance: number;
    photo?: string;
    portfolio: {
      enterprises_number: number;
      enterprises: Enterprise[];
    };
    portfolio_value: number;
    first_access: boolean;
    super_nagle: boolean;
  };
  enterprise?: string;
  success: boolean;
}
