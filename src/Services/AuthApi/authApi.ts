import { ApiInstance, ApiResponse, getApiInstance } from '~/Services/Api';
import { UserInfo } from './authModel';

export interface AccountCredentials {
  email: string;
  password: string;
}

export interface AuthApi {
  login(accountCredentials: AccountCredentials): Promise<ApiResponse<UserInfo>>;
}

export class AuthApiImpl {
  url = 'users/auth';

  constructor(private api: ApiInstance) { }

  login(accountCredentials: AccountCredentials): Promise<ApiResponse<UserInfo>> {
    return this.api.post(`${this.url}/sign_in`, accountCredentials);
  }
}

let instance: AuthApi;

export function getAuthApiInstance(api?: ApiInstance) {
  if (!instance) {
    const myApi = api || getApiInstance();
    instance = new AuthApiImpl(myApi);
  }
  return instance;
}
