import { action, ActionsUnion } from '~/Shared';
import { UserInfo, AccountCredentials } from '~/Services/AuthApi';
import { HeaderCredetials } from '.';

export enum AuthTypes {
  LOGIN_REQUEST = '[auth] LOGIN_REQUEST',
  LOGIN_SUCCESS = '[auth] LOGIN_SUCCESS',
  LOGIN_FAILURE = '[auth] LOGIN_FAILURE',
}

export const AuthCreators = {
  loginRequest: (credentials: AccountCredentials) => action(AuthTypes.LOGIN_REQUEST, credentials),
  loginSuccess: (headerCredetials: HeaderCredetials, user: UserInfo) =>
    action(AuthTypes.LOGIN_SUCCESS, { headerCredetials, user }),
  loginFailure: (errorMsg: string) => action(AuthTypes.LOGIN_FAILURE, { errorMsg }),
};

export type AuthActions = ActionsUnion<typeof AuthCreators>;
