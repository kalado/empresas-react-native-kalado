import { AppState } from '~/Redux';

export const AuthSelectors = {
  selectAuthState: ({ auth }: AppState) => auth,
  selectLoggedUser: (appState: AppState) => {
    const { user } = AuthSelectors.selectAuthState(appState);
    return user;
  },
  selectLoggedUserId: (appState: AppState) => {
    const user = AuthSelectors.selectLoggedUser(appState);
    if (user) return user.investor.id;
    return -1;
  },
  selectHeader: (appState: AppState) => {
    const { headerCredetials } = AuthSelectors.selectAuthState(appState);
    return headerCredetials;
  },
};
