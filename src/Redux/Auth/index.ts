import { UserInfo } from '~/Services/AuthApi';
import { ErrorRequestState } from '~/Services/Api/models';
import { AuthTypes, AuthActions } from './actions';

export interface HeaderCredetials {
  client: string;
  uid: string;
  access_token: string;
}

export interface AuthState extends ErrorRequestState {
  isLogged: boolean;
  isLogging: boolean;
  logoutMsg?: string;
  user?: UserInfo;
  headerCredetials?: HeaderCredetials;
}

const initialState: AuthState = {
  isLogged: false,
  isLogging: false,
  hasError: false,
  errorMsg: '',
};

export function authReducer(state = initialState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthTypes.LOGIN_REQUEST:
      return {
        ...state,
        isLogging: true,
        hasError: false,
        errorMsg: '',
      };
    case AuthTypes.LOGIN_SUCCESS:
      const { user, headerCredetials } = action.payload;
      return {
        ...state,
        user,
        headerCredetials,
        isLogged: true,
        isLogging: false,
        hasError: false,
      };
    case AuthTypes.LOGIN_FAILURE:
      const { errorMsg } = action.payload;
      return {
        ...state,
        errorMsg,
        isLogging: false,
        hasError: true,
      };
  }
  return state;
}

export * from './actions';
export * from './selectors';
