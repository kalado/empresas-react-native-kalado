import { combineReducers } from 'redux';
import { EnterpriseState, enterpriseReducer } from '~/Redux/Enterprise';

export interface RequestsState {
  enterprise: EnterpriseState;
}

const reducer = combineReducers<RequestsState>({
  enterprise: enterpriseReducer,
});

export {
  reducer as requestsReducer,
};
