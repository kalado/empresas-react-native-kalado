import { action, ActionsUnion } from '~/Shared';
import { Enterprise } from './Entity';
import { HeaderCredetials } from '../Auth';

export enum EnterpriseTypes {
  ENTERPRISE_RETRIEVE_ALL_REQUEST = '[enterprise] ENTERPRISE_RETRIEVE_ALL_REQUEST',
  ENTERPRISE_RETRIEVE_ALL_SUCCESS = '[enterprise] ENTERPRISE_RETRIEVE_ALL_SUCCESS',
  ENTERPRISE_RETRIEVE_ALL_FAILURE = '[enterprise] ENTERPRISE_RETRIEVE_ALL_FAILURE',
  ENTERPRISE_RETRIEVE_ONE_REQUEST = '[enterprise] ENTERPRISE_RETRIEVE_ONE_REQUEST',
  ENTERPRISE_RETRIEVE_ONE_SUCCESS = '[enterprise] ENTERPRISE_RETRIEVE_ONE_SUCCESS',
  ENTERPRISE_RETRIEVE_ONE_FAILURE = '[enterprise] ENTERPRISE_RETRIEVE_ONE_FAILURE',
}

export const EnterpriseRetrieveOneActionCreators = {
  enterpriseRetrieveOneRequest: (header: HeaderCredetials, enterpriseId: string) =>
    action(EnterpriseTypes.ENTERPRISE_RETRIEVE_ONE_REQUEST, { header, enterpriseId }),
  enterpriseRetrieveOneSuccess: (response: Enterprise) =>
    action(EnterpriseTypes.ENTERPRISE_RETRIEVE_ONE_SUCCESS, response),
  enterpriseRetrieveOneFailure: (errorMsg: string) =>
    action(EnterpriseTypes.ENTERPRISE_RETRIEVE_ONE_FAILURE, errorMsg),
};

export const EnterpriseRetrieveAllActionCreators = {
  enterpriseRetrieveAllRequest: (header: HeaderCredetials) =>
    action(EnterpriseTypes.ENTERPRISE_RETRIEVE_ALL_REQUEST, header),
  enterpriseRetrieveAllSuccess: (response: Enterprise[]) =>
    action(EnterpriseTypes.ENTERPRISE_RETRIEVE_ALL_SUCCESS, response),
  enterpriseRetrieveAllFailure: (errorMsg: string) =>
    action(EnterpriseTypes.ENTERPRISE_RETRIEVE_ALL_FAILURE, errorMsg),
};

export type EnterpriseRetrieveOneActions = ActionsUnion<typeof EnterpriseRetrieveOneActionCreators>;
export type EnterpriseRetrieveAllActions = ActionsUnion<typeof EnterpriseRetrieveAllActionCreators>;
export type EnterpriseActions = EnterpriseRetrieveAllActions | EnterpriseRetrieveOneActions;
