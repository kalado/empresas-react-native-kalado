import { AppState } from '~/Redux';

const EnterpriseRequestSelectors = {
  selectState: ({ requests: { enterprise } }: AppState) => enterprise,
};

const EnterpriseRetrieveSelectors = {
  selectState: (appState: AppState) => {
    const { retrieve } = EnterpriseRequestSelectors.selectState(appState);
    return retrieve;
  },
};

const EnterpriseRetrieveAllSelectors = {
  selectState: (appState: AppState) => {
    const { all } = EnterpriseRetrieveSelectors.selectState(appState);
    return all;
  },
  selectIsLoading: (appState: AppState) => {
    const { isLoading } = EnterpriseRetrieveAllSelectors.selectState(appState);
    return isLoading;
  },
};

const EnterpriseRetrieveOneSelectors = {
  selectState: (appState: AppState) => {
    const { one } = EnterpriseRetrieveSelectors.selectState(appState);
    return one;
  },
};

const EnterpriseEntitySelectors = {
  selectState: ({ entities: { enterprise } }: AppState) => enterprise,
  selectEnterpriseById: (appState: AppState, id: string) => {
    const { byId } = EnterpriseEntitySelectors.selectState(appState);
    return byId[id];
  },
  selectEnterpriseIds: (appState: AppState) => {
    const { allIds } = EnterpriseEntitySelectors.selectState(appState);
    return allIds;
  },
  selectEnterprises: (appState: AppState) => {
    const { byId, allIds } = EnterpriseEntitySelectors.selectState(appState);
    return allIds && allIds.map((id: string) => byId[id]);
  },
};

export {
  EnterpriseRetrieveOneSelectors,
  EnterpriseRetrieveAllSelectors,
  EnterpriseEntitySelectors,
};
