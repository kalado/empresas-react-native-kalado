import { Entity, EntityState } from '~/Redux/models';
import { genMapById, genAllIds } from '~/Redux/utils';
import { EnterpriseActions, EnterpriseTypes } from '~/Redux/Enterprise/actions';

interface EnterpriseType {
  id: number;
  enterprise_type_name: string;
}

export interface Enterprise extends Entity {
  email_enterprise: string;
  facebook: string;
  twitter: string;
  linkedin: string;
  phone: string;
  own_enterprise: boolean;
  enterprise_name: string;
  photo: string;
  description: string;
  city: string;
  country: string;
  value: number;
  share_price: number;
  enterprise_type: EnterpriseType;
}

export interface EnterpriseEntityState extends EntityState<Enterprise> {}

const initialState: EnterpriseEntityState = {
  byId: {},
};

export function enterpriseEntityReducer(
  state = initialState,
  action: EnterpriseActions,
): EnterpriseEntityState {
  switch (action.type) {
    case EnterpriseTypes.ENTERPRISE_RETRIEVE_ONE_SUCCESS:
      return genRetrieveSuccessState(action.payload, state);
    case EnterpriseTypes.ENTERPRISE_RETRIEVE_ALL_SUCCESS:
      return genRetrieveAllSuccessState(action.payload, state);
  }
  return state;
}

function genRetrieveSuccessState(enterpriseResponse: Enterprise, state: EnterpriseEntityState) {
  return genNewState([enterpriseResponse], state);
}

function genRetrieveAllSuccessState(
  enterpriseResponse: Enterprise[],
  state: EnterpriseEntityState,
) {
  return genNewState(enterpriseResponse, state);
}

function genNewState(enterprisesResponse: Enterprise[], state: EnterpriseEntityState) {
  return {
    ...state,
    byId: genMapById(enterprisesResponse, state),
    allIds: genAllIds(enterprisesResponse, state),
  };
}
