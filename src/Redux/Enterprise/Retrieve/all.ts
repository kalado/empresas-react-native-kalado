import { requestReducer } from '~/Shared';
import { RequestState } from '~/Services/Api/models';
import { EnterpriseActions, EnterpriseTypes } from '~/Redux/Enterprise/actions';

export interface EnterpriseRetrieveAllState extends RequestState {}

const initialState: EnterpriseRetrieveAllState = {
  isLoading: false,
  isDone: false,
  hasError: false,
  errorMsg: '',
};

export function enterpriseRetrieveAllReducer(
  state = initialState,
  action: EnterpriseActions,
): EnterpriseRetrieveAllState {
  switch (action.type) {
    case EnterpriseTypes.ENTERPRISE_RETRIEVE_ALL_REQUEST:
    case EnterpriseTypes.ENTERPRISE_RETRIEVE_ALL_SUCCESS:
    case EnterpriseTypes.ENTERPRISE_RETRIEVE_ALL_FAILURE:
      return requestReducer(state, action);
  }
  return state;
}
