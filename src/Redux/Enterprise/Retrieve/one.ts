import { requestReducer } from '~/Shared';
import { RequestState } from '~/Services/Api/models';
import { EnterpriseActions, EnterpriseTypes } from '~/Redux/Enterprise/actions';

export interface EnterpriseRetrieveOneState extends RequestState {}

const initialState: EnterpriseRetrieveOneState = {
  isLoading: false,
  isDone: false,
  hasError: false,
  errorMsg: '',
};

export function enterpriseOneRetrieveReducer(
  state = initialState,
  action: EnterpriseActions,
): EnterpriseRetrieveOneState {
  switch (action.type) {
    case EnterpriseTypes.ENTERPRISE_RETRIEVE_ONE_REQUEST:
    case EnterpriseTypes.ENTERPRISE_RETRIEVE_ONE_SUCCESS:
    case EnterpriseTypes.ENTERPRISE_RETRIEVE_ONE_FAILURE:
      return requestReducer(state, action);
  }
  return state;
}
