import { combineReducers } from 'redux';
import { EnterpriseRetrieveOneState, enterpriseOneRetrieveReducer } from './one';
import { EnterpriseRetrieveAllState, enterpriseRetrieveAllReducer } from './all';

export interface EnterpriseRetrieveState {
  all: EnterpriseRetrieveAllState;
  one: EnterpriseRetrieveOneState;
}

const reducer = combineReducers<EnterpriseRetrieveState>({
  all: enterpriseRetrieveAllReducer,
  one: enterpriseOneRetrieveReducer,
});

export {
  reducer as enterpriseRetrievelReducer,
};
