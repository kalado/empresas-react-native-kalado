import { combineReducers } from 'redux';
import { EnterpriseRetrieveState, enterpriseRetrievelReducer } from './Retrieve';

export interface EnterpriseState {
  retrieve: EnterpriseRetrieveState;
}

const reducer = combineReducers<EnterpriseState>({
  retrieve: enterpriseRetrievelReducer,
});

export * from './Retrieve';
export * from './Entity';

export {
  reducer as enterpriseReducer,
};
