import { AppState } from '~/Redux';

export const StartupSelectors = {
  selectState: ({ startup }: AppState) => startup,
  selectIsDone: (appState: AppState) => {
    const { isDone } = StartupSelectors.selectState(appState);
    return isDone;
  },
};
