import { action, ActionsUnion } from '~/Shared';

export enum StartupTypes {
  STARTUP_START = 'STARTUP_START',
  STARTUP_FINISH = 'STARTUP_FINISH',
}

export const StartupCreators = {
  startupStart: () => action(StartupTypes.STARTUP_START),
  startupFinish: () => action(StartupTypes.STARTUP_FINISH),
};

export type StartupActions = ActionsUnion<typeof StartupCreators>;
