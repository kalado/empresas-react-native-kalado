import { StartupActions, StartupTypes } from './actions';

export interface StartupState {
  isDone: boolean;
}

const initialState: StartupState = {
  isDone: false,
};

export function startupReducer(state = initialState, action: StartupActions): StartupState {
  switch (action.type) {
    case StartupTypes.STARTUP_FINISH:
      return {
        ...state,
        isDone: true,
      };
  }
  return state;
}

export * from './actions';
