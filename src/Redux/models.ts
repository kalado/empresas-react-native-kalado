export interface Entity {
  id: string;
}

export interface EntityMap<T extends Entity> {
  [id: string]: T;
}

export interface EntityState<T extends Entity> {
  byId: EntityMap<T>;
  allIds?: string[];
}
