import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from '@redux-saga/core';

import { rootSaga } from '~/Saga';
import { Reactotron } from '~/Config';
import { Apis, getApisInstance } from '~/Services';
import { StartupState, startupReducer, StartupCreators } from './Startup';
import { AuthState, authReducer } from './Auth';
import { EntitiesState, entitiesReducer } from './Entities';
import { RequestsState, requestsReducer } from './Requests';

export interface AppState {
  startup: StartupState;
  auth: AuthState;
  entities: EntitiesState;
  requests: RequestsState;
}

export const allReducers = combineReducers<AppState>({
  startup: startupReducer,
  auth: authReducer,
  entities: entitiesReducer,
  requests: requestsReducer,
});

export function createAppStore() {
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(
    allReducers,
    compose(applyMiddleware(sagaMiddleware), Reactotron.createEnhancer!()),
  );

  const apis: Apis = getApisInstance();

  sagaMiddleware.run(rootSaga, apis);

  store.dispatch(StartupCreators.startupStart());

  return { store };
}
