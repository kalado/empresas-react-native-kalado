import { Entity, EntityState } from './models';

function genMapById<T extends Entity>(data: T[] = [], { byId }: EntityState<T>) {
  return data
    .reduce(
      (acc, curr: T) => {
        acc[curr.id] = curr;
        return acc;
      },
      byId || {});
}

function genAllIds<T extends Entity>(
  data: T[] = [],
  { allIds }: EntityState<T>,
  reset?: boolean,
) {
  const previousIds = reset ? [] : (allIds || []);
  return [...new Set([...previousIds, ...data.map(d => `${d.id}`)])];
}

export {
  genMapById,
  genAllIds,
};
