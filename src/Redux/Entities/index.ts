import { combineReducers } from 'redux';
import { EnterpriseEntityState, enterpriseEntityReducer } from '../Enterprise';

export interface EntitiesState {
  enterprise: EnterpriseEntityState;
}

const reducer = combineReducers<EntitiesState>({
  enterprise: enterpriseEntityReducer,
});

export {
  reducer as entitiesReducer,
};
