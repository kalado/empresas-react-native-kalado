const NODE_ENV = process.env.NODE_ENV;

const DebugConfig = {
  yellowBox: __DEV__,
  dev: NODE_ENV === 'development',
  test: NODE_ENV === 'test',
};

export {
  DebugConfig,
};
