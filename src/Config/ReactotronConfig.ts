import Reactotron from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';

import { DebugConfig } from './DebugConfig';
import { appIp } from './config.json';

let reactotron: typeof Reactotron;
if (DebugConfig.dev) {
  reactotron = Reactotron
    .configure({ host: appIp })
    .useReactNative({})
    .use(reactotronRedux())
    .connect();
}

console.disableYellowBox = !DebugConfig.yellowBox;

export {
  reactotron as Reactotron,
};
