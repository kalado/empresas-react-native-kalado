import React, { useState } from 'react';
import { Text, TextInput, Button } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';

import { AuthCreators, AuthSelectors } from '~/Redux/Auth';
import { useOnDataErrorEffect, useOnDataSuccessEffect, addNavigationOptions } from '~/Shared';
import { Alert, SafeAreaView } from 'react-native';
import { useNavigateToRoute } from '~/Shared/navigationHooksUtils';

function LoginScreen() {
  const { navigateToWelcome } = useNavigateToRoute();
  const dispatch = useDispatch();
  function handleLogIn() {
    dispatch(AuthCreators.loginRequest({ email, password }));
  }

  const { isLogged, hasError, errorMsg, isLogging } = useSelector(AuthSelectors.selectAuthState);
  useOnDataErrorEffect(
    () => {
      Alert.alert('Error', errorMsg);
    },
    { hasError, errorMsg },
  );

  useOnDataSuccessEffect(
    () => {
      setEmail('');
      setPassword('');
      navigateToWelcome();
    },
    { hasError, isDone: isLogged },
  );

  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  return (
    <SafeAreaView>
      <Text>Entre com seu Login e Senha</Text>
      {!isLogged && (
        <>
          <TextInput
            placeholder="E-mail"
            value={email}
            onChangeText={setEmail}
            keyboardType="email-address"
          />
          <TextInput
            placeholder="Senha"
            value={password}
            onChangeText={setPassword}
            secureTextEntry={true}
          />
          <Button loading={isLogging} mode="contained" onPress={handleLogIn}>
            Acessar
          </Button>
        </>
      )}
    </SafeAreaView>
  );
}
addNavigationOptions(LoginScreen, () => ({
  title: 'Login',
}));

export {
  LoginScreen,
};
