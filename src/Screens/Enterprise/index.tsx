import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';

import { addNavigationOptions } from '~/Shared';
import { useSelector } from 'react-redux';
import {
  EnterpriseEntitySelectors,
} from '~/Redux/Enterprise/selectors';
import { Card, Divider, List } from 'react-native-paper';
import { NavigationScreenProps } from 'react-navigation';
import { AppState } from '~/Redux';

interface Props extends NavigationScreenProps { }

function EnterpriseScreen({ navigation }: Props) {
  const id = navigation.getParam('id');
  const enterprise = useSelector(
    (appState: AppState) => EnterpriseEntitySelectors.selectEnterpriseById(appState, id),
  );
  return (
    <SafeAreaView>
      <ScrollView>
        {enterprise &&
          <>
            <Card>
              <Card.Title
                title={enterprise.enterprise_name}
                subtitle={enterprise.description}
              />
              <Card.Cover
                source={{ uri: `http://empresas.ioasys.com.br/${enterprise.photo}` }}
              />
              <Card.Content>
                <List.Item title="Nome" description={enterprise.enterprise_name} />
                <List.Item
                  title="Tipo"
                  description={enterprise.enterprise_type.enterprise_type_name}
                />
                <List.Item title="E-mail" description={`${enterprise.email_enterprise}`} />
                <List.Item title="Telefone" description={`${enterprise.phone}`} />
                <List.Item title="Facebook" description={`${enterprise.facebook}`} />
                <List.Item title="Twitter" description={`${enterprise.twitter}`} />
                <List.Item title="LinkedIn" description={`${enterprise.linkedin}`} />
                <List.Item title="Cidade" description={`${enterprise.city}`} />
                <List.Item title="País" description={`${enterprise.country}`} />
                <List.Item title="Preço divulgado" description={`${enterprise.share_price}`} />
                <List.Item title="Valor" description={`${enterprise.value}`} />
                <List.Item title="Própria" description={`${enterprise.own_enterprise}`} />
              </Card.Content>
            </Card>
            <Divider />
          </>
        }
      </ScrollView>
    </SafeAreaView>
  );
}
addNavigationOptions(EnterpriseScreen, () => ({
  title: 'Empresa',
}));

export {
  EnterpriseScreen,
};
