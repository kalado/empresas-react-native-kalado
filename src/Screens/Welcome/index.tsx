import React from 'react';
import { SafeAreaView, Alert, ScrollView, View } from 'react-native';

import { addNavigationOptions, useDidMountEffect, useOnDataErrorEffect } from '~/Shared';
import { useDispatch, useSelector } from 'react-redux';
import { EnterpriseRetrieveAllActionCreators } from '~/Redux/Enterprise/actions';
import {
  EnterpriseRetrieveAllSelectors,
  EnterpriseEntitySelectors,
} from '~/Redux/Enterprise/selectors';
import { Card, Paragraph, Button, Divider } from 'react-native-paper';
import { AuthSelectors } from '~/Redux/Auth';
import { useNavigateToRoute } from '~/Shared/navigationHooksUtils';

function WelcomeScreen() {
  const { navigateToLogin, navigateToEnterprise } = useNavigateToRoute();
  const dispatch = useDispatch();
  const { hasError, errorMsg } = useSelector(
    EnterpriseRetrieveAllSelectors.selectState,
  );

  const headerCredetials = useSelector(AuthSelectors.selectHeader);

  useOnDataErrorEffect(
    () => {
      Alert.alert('Error', errorMsg);
    },
    { hasError, errorMsg },
  );

  function loadingEnterprises() {
    if (headerCredetials) {
      dispatch(EnterpriseRetrieveAllActionCreators.enterpriseRetrieveAllRequest(headerCredetials));
    } else {
      navigateToLogin();
    }
  }

  useDidMountEffect(loadingEnterprises);
  const enterprises = useSelector(
    EnterpriseEntitySelectors.selectEnterprises,
  );

  function handlePress(id: string) {
    navigateToEnterprise(id);
  }

  return (
    <SafeAreaView>
      <ScrollView>
        {enterprises && enterprises.map((enterprise) => {
          return (
            <View key={enterprise.id}>
              <Card>
                <Card.Title
                  title={enterprise.enterprise_name}
                  subtitle={enterprise.enterprise_type.enterprise_type_name}
                />
                <Card.Cover
                  source={{ uri: `http://empresas.ioasys.com.br/${enterprise.photo}` }}
                />
                <Card.Content>
                  <Paragraph>{enterprise.description}</Paragraph>
                </Card.Content>
                <Card.Actions>
                  <Button onPress={() => handlePress(enterprise.id)}>Visualizar</Button>
                </Card.Actions>
              </Card>
              <Divider />
            </View>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
}
addNavigationOptions(WelcomeScreen, () => ({
  title: 'Empresas',
}));

export {
  WelcomeScreen,
};
