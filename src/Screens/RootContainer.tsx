import React from 'react';
import { StatusBar } from 'react-native';

import { AppContainer } from '~/Navigation';

function RootContainer() {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <AppContainer />
    </>
  );
}

export {
  RootContainer,
};
