import React from 'react';
import { Provider } from 'react-redux';
import { Provider as PaperProvider } from 'react-native-paper';

import { DebugConfig, Reactotron } from '~/Config';
import { createAppStore } from '~/Redux';
import { RootContainer } from './RootContainer';

const { store } = createAppStore();

function App() {
  return (
    <Provider store={store}>
      <PaperProvider>
        <RootContainer />
      </PaperProvider>
    </Provider>
  );
}

if (DebugConfig.dev) {
  Reactotron.overlay(App);
}

export {
  App,
};
