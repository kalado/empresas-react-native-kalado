import { useRef, useEffect, EffectCallback } from 'react';

import { RequestState, ErrorRequestState, ErrorRequestStateWithData } from '~/Services/Api/models';

function useDidMountEffect(fn: EffectCallback) {
  useEffect(fn, []);
}

function useDidUpdateEffect(fn: Function, inputs: any[]) {
  const didMountRef = useRef(false);
  useEffect(
    () => {
      if (!didMountRef.current) {
        didMountRef.current = true;
        return;
      }
      fn();
    },
    inputs,
  );
}

function useOnDataSuccessEffect(
    fn: Function, { isDone, hasError }: Pick<RequestState, 'isDone' | 'hasError'>) {
  useDidUpdateEffect(
    () => {
      if (isDone && !hasError) fn();
    },
    [isDone, hasError],
  );
}

function useOnDataErrorEffect(fn: Function, { hasError, errorMsg }: ErrorRequestState) {
  useDidUpdateEffect(
    () => {
      if (hasError && errorMsg) fn();
    },
    [hasError, errorMsg],
  );
}

function useLoadDataErrorEffect(
  fn: Function, { data, hasError, errorMsg }: ErrorRequestStateWithData,
) {
  useDidUpdateEffect(
    () => {
      if (data && hasError && errorMsg) fn();
    },
    [data, hasError, errorMsg],
  );
}

export {
  useDidMountEffect,
  useDidUpdateEffect,
  useOnDataSuccessEffect,
  useOnDataErrorEffect,
  useLoadDataErrorEffect,
};
