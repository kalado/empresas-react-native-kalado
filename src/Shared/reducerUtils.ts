import { ActionWithPayload, Action, isActionWithPayload } from '~/Shared/ReduxActionHelpers';
import { RequestState, Pageable } from '~/Services/Api/models';

type _REQUEST = '_REQUEST';
type _SUCCESS = '_SUCCESS';
type _FAILURE = '_FAILURE';

export function isRequest(actionType: string): actionType is _REQUEST {
  return actionType.endsWith('_REQUEST');
}
export function isSuccess(actionType: string): actionType is _SUCCESS {
  return actionType.endsWith('_SUCCESS');
}
export function isFailure(actionType: string): actionType is _FAILURE {
  return actionType.endsWith('_FAILURE');
}

function extractErrorMsgFromPayload(action: ActionWithPayload<string, any>): string {
  let errorMsg = undefined;
  const { payload } = <ActionWithPayload<string, any>>action;
  if (typeof payload === 'string') {
    errorMsg = payload;
  } else if (typeof payload === 'object') {
    if (payload.errorMsg && typeof payload.errorMsg === 'string') {
      errorMsg = payload.errorMsg;
    } else if (payload.message && typeof payload.message === 'string') {
      errorMsg = payload.message;
    }
  }
  return errorMsg;
}

export function requestReducer(
  state: RequestState,
  action: Action<string> | ActionWithPayload<string, any>,
) {
  const { type } = action;
  if (isRequest(type)) {
    return {
      ...state,
      isLoading: true,
      hasError: false,
      errorMsg: '',
      isDone: false,
    };
  }
  if (isSuccess(type)) {
    return {
      ...state,
      isLoading: false,
      hasError: false,
      errorMsg: '',
      isDone: true,
    };
  }
  if (isFailure(type)) {
    if (!isActionWithPayload(action)) {
      throw new Error('Payload obrigatório para requestReducer _FAILURE');
    }
    const errorMsg = extractErrorMsgFromPayload(action);
    if (!errorMsg) {
      throw new Error('Payload deve conter \'errorMsg\' para requestReducer _FAILURE');
    }
    return {
      ...state,
      errorMsg,
      isLoading: false,
      hasError: true,
      isDone: true,
    };
  }
  return state;
}

export interface LoadPageableState extends RequestState {
  pageable: Pageable;
}

export function requestPageableReducer<T extends LoadPageableState>(
  state: T,
  action: ActionWithPayload<string, any>,
  initialState: T,
): T {
  if (isRequest(action.type)) {
    return requestCallPageableReducer<T>(state, action, initialState);
  }
  if (isSuccess(action.type)) {
    return requestSuccessPageableReducer<T>(state, action);
  }
  if (isFailure(action.type)) {
    return requestFailurePageableReducer<T>(state, action);
  }
  return state;
}

function requestCallPageableReducer<T extends LoadPageableState>(
  state: T,
  action: ActionWithPayload<string, any>,
  initialState: T,
) {
  if (!isActionWithPayload(action)) {
    throw new Error('Payload obrigatório para requestCallPageableReducer');
  }
  const { payload: { reset } } = action;
  if (typeof reset !== 'boolean') {
    throw new Error('Payload deve possuir \'reset\' boolean para requestCallPageableReducer');
  }
  const pageable = reset ? initialState.pageable : state.pageable;
  return {
    ...state,
    ...requestReducer(state, action),
    pageable: {
      ...pageable,
    },
  } as T;
}

function requestSuccessPageableReducer<T extends LoadPageableState>(
  state: T,
  action: ActionWithPayload<string, any>,
) {
  if (!isActionWithPayload(action)) {
    throw new Error('Payload obrigatório para requestSuccessPageableReducer');
  }
  const { payload: { data } } = action;
  if (typeof data === 'undefined') {
    throw new Error('Payload deve possuir \'data\' para requestSuccessPageableReducer');
  }
  const dataSuccesss = data || [];
  const countDataSuccess = dataSuccesss.length;
  const { pageable: { page, size } } = state;
  const newPage = countDataSuccess <= size ? page + 1 : page;
  return {
    ...requestReducer(state, action),
    pageable: {
      ...state.pageable,
      page: newPage,
    },
  } as T;
}

function requestFailurePageableReducer<T extends LoadPageableState>(
  state: T,
  action: ActionWithPayload<string, any>,
) {
  return {
    ...state,
    ...requestReducer(state, action),
  } as T;
}
