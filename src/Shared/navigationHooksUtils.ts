import { useNavigation } from 'react-navigation-hooks';

import { appRoutes } from '~/Navigation/routes';

interface NavigationToRoute {
  navigateToWelcome: () => void;
  navigateToLogin: () => void;
  navigateToEnterprise: (id: string) => void;
}

function useNavigateToRoute(): NavigationToRoute {
  const { navigate } = useNavigation();
  return {
    navigateToWelcome: () => navigate(appRoutes.Welcome),
    navigateToLogin: () => navigate(appRoutes.Login),
    navigateToEnterprise: (id: string) => navigate(appRoutes.Enterprise, {
      ['id']: id,
    }),
  };
}

export {
  useNavigateToRoute,
};
