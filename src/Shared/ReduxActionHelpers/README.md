# Redux Action Helpers

Tipos e funções utilitárias para auxiliar na criação de Action Creators para usar com redux.

#### Retirado do blog:

[https://medium.com/@martin_hotell/improved-redux-type-safety-with-typescript-2-8-2c11a8062575)](https://medium.com/@martin_hotell/improved-redux-type-safety-with-typescript-2-8-2c11a8062575)

## Usage

```typescript
import { action, ActionsUnion } from '.';

enum Types {
  INCREMENT = '[sample] INCREMENT',
  DECREMENT = '[sample] DECREMENT',
}

const Creators = {
  simpleIncrement: () => action(Types.INCREMENT),
  simpleDecrement: () => action(Types.DECREMENT),
};

export {
  Creators as SampleCreators,
  Types as SampleTypes,
};

export type SampleActionTypes = ActionsUnion<typeof Creators>;
```