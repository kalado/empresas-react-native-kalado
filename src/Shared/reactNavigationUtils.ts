import React from 'react';
import { NavigationScreenOptions, NavigationScreenProps } from 'react-navigation';
import hoistNonReactStatics from 'hoist-non-react-statics';

function addNavigationOptions(
  component: React.ComponentType<any>,
  fn: (props: NavigationScreenProps) => NavigationScreenOptions,
) {
  const A = () => null;
  A.navigationOptions = fn;
  hoistNonReactStatics(component, A);
}

export {
  addNavigationOptions,
};
